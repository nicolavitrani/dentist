<x-layout>
<x-navbar></x-navbar>
    @push('title')
    Homepage
    @endpush

    <div class="container-fluid d-empty-space">

    </div>


        {{-- Carosello --}}

        <x-carousel>

        </x-carousel>

        {{-- h1 --}}

        <div class="container mt-5 mb-5">
            <div class="row">
                <div class="col-12">
                    <h1 class="text-center display-3">
                      Studi dentistici Dottor Marco Rossi
                    </h1>
                </div>
            </div>
        </div>

        <div class="container mt-6 mb-6">
            <div class="row d-flex justify-content-between mt-4 mb-4">
                <div class="col-12 col-md-12 col-lg-5 align-items-center">
                    <p>
                        <img src="/img/marco-rossi.png" alt="" class="img-fluid">
                    </p>
                </div>

                <div class="col-12 col-md-12 col-lg-5 align-items-center my-auto mt-5">
                    <p>
                        Negli Studi Dentistici Dr. Rossi puoi affrontare le cure odontoiatriche con la massima tranquillità. Le nuove apparecchiature e i software di ultima generazione rendono gli interventi più semplici e meno invasivi, con standard terapeutici d’eccellenza.
                        La nostra équipe riunisce alcuni dei migliori specialisti del settore a livello nazionale.
                    </p>
                    <p>
                        “Ogni nostro paziente deve poter vivere l’appuntamento come un’esperienza di benessere da concedersi in piena serenità: un’occasione per prendersi cura di sé, con la certezza di trovarsi in buone mani.”
                    </p>

                    <p class="text-center">
                        <a href="{{route('staff')}}" class="btn d-button">Scopri il nostro Staff</a>
                    </p>
                </div>
            </div>
        </div>



      {{-- Servizi --}}

      <div class="container-fluid mt-5 mb-0 d-bg-gray">
          <div class="row">
              <div class="col-12">
                  <h2 class="text-center pt-4 pb-4">
                    I nostri servizi
                  </h2>
              </div>
          </div>
      </div>

      <div class="container-fluid mt-0 mb-3 d-bg-gray">
            <div class="row justify-content-center">
                @foreach ($services as $service)


                    <div class="col-12 col-md-5 col-lg-3 mt-4 mb-4">

                        <x-card
                        title="{{$service['title']}}"
                        serviceDescription="{{$service['serviceDescription']}}"
                        img="{{$service['img']}}"
                        route="{{route('dettaglioServizi', ['title'=>$service['title'] ])}}"
                        />

                    </div>

                @endforeach
            </div>
     </div>

<x-footer></x-footer>
</x-layout>
