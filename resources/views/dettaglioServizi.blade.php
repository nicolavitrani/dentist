<x-layout>
    @push('title')
    Dettaglio Servizi
    @endpush
    <x-navbar></x-navbar>

    <div class="container-fluid d-empty-space">

    </div>

    <div class="container mt-6 mb-6">
        <div class="row">
            <div class="col-12">
                <h2 class="text-center">
                  {{$service['title']}}
                </h2>
            </div>
        </div>
    </div>

    <div class="container mt-6 mb-3">
        <div class="row d-flex justify-content-between mt-4 mb-4">
            <div class="col-12 col-md-12 col-lg-5 justify-content-center">
                <p class="text-center">
                    {{$service['serviceDescription']}}
                </p>
            </div>

            <div class="col-12 col-md-12 col-lg-5 justify-content-center">
                <p class="text-center">
                    {{$service['serviceDescription2']}}
                </p>
            </div>
        </div>
    </div>

    <div class="container mt-6 mb-6">
        <div class="row">
            <div class="col-12 text-center">
                <a href="{{route('homepage')}}" class="btn d-button">Torna alla Home</a>
            </div>
        </div>
    </div>

<x-footer></x-footer>
</x-layout>
