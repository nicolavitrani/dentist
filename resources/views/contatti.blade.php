<x-layout>
    @push('title')
    Contatti
    @endpush
<x-navbar></x-navbar>

    <div class="container-fluid d-empty-space">

    </div>


    <div class="container">
        <div class="row justify-content-around">
            <div class="col-12 col-md-4 my-auto">
                <h4 class="text-center">
                    È il tuo primo appuntamento?
                </h4>

                <p class="text-justify mt-3">
                    Dalle attività specialistiche più complesse alle semplici operazioni di routine, negli Studi Dentistici Dr. Rossi trovi un team di professionisti pronto a offrirti il miglior livello di prestazioni in ogni tipologia di intervento.
                </p>
                <p class="text-justify">
                    <strong>Prenota la tua visita senza impegno.</strong>
                </p>
            </div>

            <div class="col-12 col-md-4 my-auto">

                <form method="POST" action="{{route('contacts.submit')}}">
                    @csrf
                    <div class="mb-3">
                      <label for="exampleInputEmail1" class="form-label">Email</label>
                      <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="email">

                    </div>
                    <div class="mb-3">
                      <label for="exampleInputPassword1" class="form-label">Nome e cognome</label>
                      <input type="text" class="form-control" id="exampleInputPassword1" name="user">
                    </div>
                    <div class="mb-3">
                        <textarea name="message" cols="30" rows="10" placeholder="Inserisci un messaggio"></textarea>
                    </div>
                    <button type="submit" class="btn d-button">Invia</button>
                  </form>

            </div>
        </div>
    </div>

    <div class="container mt-5">
        <div class="row mt-5">
            <div class="col-12">
                @if (session('message'))
                    <div class="alert alert-success">
                        {{session('message')}}
                    </div>
                @endif
            </div>
        </div>
    </div>

<x-footer></x-footer>
</x-layout>
