

  <div class="container-fluid">
      <div class="row">
          <div class="col-12">
            <nav class="navbar navbar-expand-lg navbar-light d-nav-background fixed-top">
                <div class="container">
                  <a class="navbar-brand" href="{{route('homepage')}}"><img style="height: 100px" src="/img/logo.png" alt="logo"> DR ROSSI</a>
                  <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                      </button>
                  <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="{{route('homepage')}}">Home</a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" href="{{route('staff')}}">Staff</a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" href="{{route('doveSiamo')}}">Dove siamo</a>
                          </li>
                          <li class="nav-item dropdown">
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" href="{{route('contatti')}}" tabindex="-1" aria-disabled="true">Contatti</a>
                          </li>
                          {{-- pulsante per attivare la dark mode --}}
                          {{-- <li class="nav-item">
                            <button class="d-button" onclick="myFunction()">Toggle dark mode</button>
                          </li> --}}

                    </ul>
                  </div>
                </div>
              </nav>
          </div>
      </div>
  </div>
