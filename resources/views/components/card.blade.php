{{-- <div class="col-12 col-md-6 col-lg-3"> --}}
    <div class="card mt-6 mx-auto" style="width: 18rem; height: 500px">
        <img src="{{$img}}" class="card-img-top mb-3" style="height: 150px" alt="{{$title}}">
        <div class="card-body text-center mt-3">
            <h5 class="card-title text-center text-white">{{$title}}</h5>
            <p class="card-text text-center mb-3 text-white">{{$serviceDescription}}</p>
            <a href="{{$route}}" class="btn d-button mt-3">Scopri di più</a>
        </div>
    </div>
{{-- </div> --}}
