{{-- Carosello - prima opzione --}}

{{-- <div class="container-fluid h-75">
    <div class="row">
        <div class="col-12">

            <div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel">
                <div class="carousel-indicators">
                  <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                  <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
                  <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></button>
                  <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 4"></button>
                </div>
                <div class="carousel-inner">
                    <div class="carousel-item active">
                    <img src="/img/dentistRoom.jpg" class="d-block img-fluid" alt="sala dentista"> --}}

                    {{-- <div class="carousel-caption d-none d-md-block mb-5 d-bg-text-carousel">
                      <h3 class="d-carousel-text text-black">Ogni spazio è progettato pensando a te</h3>
                      <p class="text-black">FARTI STAR BENE È IL NOSTRO OBIETTIVO</p>
                    </div>
                  </div>
                  <div class="carousel-item">
                    <img src="/img/equipe.jpg" class="d-block img-fluid" alt="squadra">

                    <div class="carousel-caption d-none d-md-block mb-5 d-bg-text-carousel">
                      <h3 class="d-carousel-text text-black">La migliore équipe per ridare smalto</h3>
                      <h3 class="d-carousel-text text-black">ai tuoi denti</h3>
                      <p class="text-black">IGIENE ORALE PROFESSIONALE</p>
                    </div>
                  </div>
                  <div class="carousel-item">
                    <img src="/img/dentist.jpg" class="d-block img-fluid" alt="dentista">

                    <div class="carousel-caption d-none d-md-block mb-5 d-bg-text-carousel">
                      <h3 class="d-carousel-text text-black">Qui, ogni paziente è al primo posto</h3>
                      <p class="text-black">UN SORRISO DA CAMPIONI CON I PARADENTI ORAL-B®</p>
                    </div>
                  </div>
                    <div class="carousel-item">
                        <img src="/img/sunrise.jpg" class="d-block img-fluid" alt="sorriso">

                        <div class="carousel-caption d-none d-md-block mb-5 d-bg-text-carousel">
                        <h3 class="d-carousel-text text-black">La salute del tuo sorriso è in buone mani</h3>
                        <p class="text-black">CI PRENDIAMO CURA DI TE CON TRATTAMENTI PERSONALIZZATI</p>
                        </div>
                    </div>
                  </div>
                </div>
                <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
                    <i class="fas fa-arrow-alt-circle-left display-5"></i>
                <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
                    <i class="fas fa-arrow-alt-circle-right display-5"></i>
                </button>
              </div>

        </div>
    </div>
</div> --}}

{{-- Carosello - seconda opzione --}}

<div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="carousel">
    <div class="carousel-indicators">
      <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
      <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
      <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2" aria-label="Slide 3"></button>
    </div>
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="/img/dentist.jpg" class="d-block w-100" alt="dentista">
      </div>
      <div class="carousel-item">
        <img src="/img/equipe.jpg" class="d-block w-100" alt="equipe">
      </div>
      <div class="carousel-item">
        <img src="/img/sunrise.jpg" class="d-block w-100" alt="sunrise">
      </div>
    </div>
    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="visually-hidden">Previous</span>
    </button>
    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="visually-hidden">Next</span>
    </button>
  </div>
