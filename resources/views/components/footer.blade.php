<div class="container-fluid d-footer">
    <div class="row mt-5 mt-0 justify-content-center">
        <div class="col-12 d-bg-clear">
            <p class="text-white text-center" href=""><img style="height: 200px" src="/img/logo.png" alt="logo"></p>
        </div>
        <div class="col-12 col-md-6">

                <p class="text-center text-white my-3">
                    Studi Dentistici Dr. Marco Rossi: <br>
                    Viale della libertà, 44F – 36100 VICENZA <br>
                    Via VIII Febbraio, 2035121 PADOVA
                </p>


        </div>
        <div class="col-12 col-md-6">

            <p class="text-center text-white mt-3">
                Seguici sui social! <br>
                <i class="fab fa-facebook"></i>  <i class="fab fa-instagram"></i>  <i class="fab fa-twitter"></i> <br>
                P.IVA 01502620386 • PRIVACY • COOKIE • CREDITS
            </p>

        </div>
    </div>

</div>
