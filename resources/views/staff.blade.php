<x-layout>
    @push('title')
    Staff
    @endpush
    <x-navbar></x-navbar>
    <div class="container-fluid d-empty-space d-bg-gray">

    </div>

    <div class="container-fluid mt-6 mb-6">
        <div class="row d-bg-gray">
            <div class="col-12 mb-5">
                <h2 class="text-center">
                  Il nostro staff
                </h2>
            </div>
        </div>
    </div>

    <div class="container-fluid mb-6">
        <div class="row justify-content-around d-bg-gray">
            <div class="col-12 col-md-5 my-auto">
                <p class="text-center display-6">
                    Affidati ad una squadra di professionisti
                </p>

                <p class="text-center">
                    Il nostro team specializzato è sempre pronto ad affrontare nuove sfide
                </p>
            </div>
            <div class="col-12 col-md-5">
                <img class="img-fluid" src="/img/team.png" alt="team">
            </div>
        </div>
    </div>

    @foreach ($doctors as $doctor)

        <div class="container">
            <div class="row mt-6 mb-6 justify-content-around">
                <div class="col-12 col-md-5 my-auto">
                    <img class="img-fluid mt-5 mb-5" src="{{$doctor['img']}}" alt="foto">
                </div>
                <div class="col-12 col-md-5 my-auto">
                    <h3 class="text-center">
                        {{$doctor['name']}} {{$doctor['surname']}}
                    </h3>
                    <p class="text-center">
                        {{$doctor['role']}}
                    </p>
                    <p class="text-center">
                        {{$doctor['detail']}}
                    </p>
                </div>
            </div>
        </div>

    @endforeach


<x-footer></x-footer>
</x-layout>
