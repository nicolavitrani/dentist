<x-layout>
    @push('title')
    Dove siamo
    @endpush
<x-navbar></x-navbar>

    <div class="container">
        <div class="row d-map">
            <div class="col-12 mt-2 mb-2">
                <h2 class="text-center">I nostri studi dentistici</h2>
            </div>
        </div>
    </div>

    <div class="container mt-5">
        <div class="row mt-5 mb-5 justify-content-center  align-items-center text-center">
            <div class="col-12 col-md-12 col-lg-6 justify-content-center">
                <iframe class="img-fluid" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d5588.17068747159!2d11.542888828699915!3d45.54860869627345!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x477f317de4d2516f%3A0x6a8ec0930b8440bc!2s36100%20Vicenza%2C%20Province%20of%20Vicenza!5e0!3m2!1sen!2sit!4v1621159628851!5m2!1sen!2sit" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
            </div>

            <div class="col-12 col-md-12 col-lg-6 mt-5">
                <h3 class="text-center">Studio dentistico di Vicenza</h3>
                <h5 class="text-center">Viale della libertà, 44F – 36100 VICENZA</h5>

                <p class="text-center">
                    <i class="far fa-calendar-check"></i>
                </p>
                <p class="text-center">
                    <strong> Dal Lunedì al Venerdì </strong>
                </p>

                <p class="text-center">
                    09.00 - 13:00
                </p>

                <p class="text-center">
                    15.00 - 19.30
                </p>
                <div class="d-flex justify-content-center">
                    <button class="mt-3 d-button">
                        Chiamaci
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="container mt-5">
        <div class="row mt-5 mb-5 justify-content-center  align-items-center">

            <div class="col-12 col-md-12 col-lg-6 order-2 mt-5">
                <h3 class="text-center">Studio dentistico di Padova</h3>
                <h5 class="text-center">Via VIII Febbraio, 2035121 PADOVA</h5>

                <p class="text-center">
                    <i class="far fa-calendar-check"></i>
                </p>
                <p class="text-center">
                    <strong> Dal Lunedì al Venerdì </strong>
                </p>

                <p class="text-center">
                    09.00 - 13:00
                </p>

                <p class="text-center">
                    15.00 - 19.30
                </p>

                <div class="d-flex justify-content-center">
                    <button class="mt-3 d-button">
                        Chiamaci
                    </button>
                </div>
            </div>
            <div class="col-12 col-md-12 col-lg-6 justify-content-center text-center">
                <iframe class="img-fluid" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2801.0734932872256!2d11.87548521586746!3d45.407857545221596!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x477eda50791cc905%3A0x7a74d0b971e6e08c!2sVia%20VIII%20Febbraio%2C%2020%2C%2035121%20Padova%20PD!5e0!3m2!1sen!2sit!4v1621176745879!5m2!1sen!2sit" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
            </div>

        </div>
    </div>



    <x-footer></x-footer>
</x-layout>
