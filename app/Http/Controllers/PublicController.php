<?php

namespace App\Http\Controllers;

use App\Mail\ContactMail;
use App\Models\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class PublicController extends Controller
{
    public function homepage(){

        $services = [
            ['title' => 'Prevenzione', 'serviceDescription' => 'Igiene orale professionale, sigillatura, bite per il bruxismo, Skaling (currettaggio) e levigatura radicolare.', 'img' => '/img/prevenzione.jpeg'],
            ['title' => 'Odontoiatria', 'serviceDescription' => 'Otturazione, devitalizzazione, corona, ponte, impianto, estrazione dei denti del giudizio, chirurgia semplice.', 'img' => '/img/odontoiatria.jpeg'],
            ['title' => 'Odontoiatria estetica e ricostruttiva', 'serviceDescription' => 'Sbiancamento dentale, faccetta in ceramica, otturazione estetica, protesi.', 'img' => '/img/odontoiatri estetica.jpeg'],
            ['title' => 'Apparecchi', 'serviceDescription' => 'Apparecchio tradizionale esterno, apparecchio invisibile interno, nuvola, paradenti D-FENDER®', 'img' => '/img/apparecchi.jpeg'],
        ];

        return view('homepage', compact('services'));
    }

    public function dettaglioServizi($title) {

        $services = [
            ['title' => 'Prevenzione', 'serviceDescription' => 'Se vuoi avere un sorriso sano, ricorda di sottoporti regolarmente alla pulizia e alla lucidatura dei denti: è il modo migliore per prevenire le principali patologie del cavo orale, come carie, gengiviti o piorrea.
            Lavarsi i denti, infatti, può non bastare per rimuovere in profondità la placca, il tartaro e i batteri che si formano nei punti più difficili da raggiungere con lo spazzolino. La sigillatura è raccomandata per prevenire la carie nei bambini.
            All’inizio, quando prendono il posto dei denti da latte, i denti permanenti hanno solchi profondi e difficili da pulire. Il risultato? I batteri proliferano indisturbati generando la carie, soprattutto se l’igiene è carente.
            La sigillatura serve proprio a scongiurare questo rischio, grazie a una resina che chiude le fessure. È una procedura veloce, indolore e priva di controindicazioni .', 'img' => '/img/prevenzione.jpeg', 'serviceDescription2' => 'Ti è mai capitato di digrignare i denti? Succede a chi, soprattutto di notte, scarica sulla mandibola parte dello stress accumulato durante la giornata.
            La conseguenza del bruxismo è duplice: tensione muscolare e usura dei denti per effetto dei micro-traumi da contatto.
            Questo disturbo è risolvibile indossando il bite, un dispositivo mobile concepito per proteggere il tuo sorriso: è modellato sul calco dei denti e ti aiuta a rilassare la muscolatura. Questo trattamento non chirurgico sotto anestesia rimuove la placca e i residui che si depositano tra la radice e la gengiva, smussando inoltre le irregolarità.
            Si impedisce così che il solco gengivale formi una tasca dove i batteri possano proliferare.
            Se hai una predisposizione a questa patologia ti consigliamo di sottoporti al trattamento con una certa regolarità: l’accumulo di tartaro.'],
            ['title' => 'Odontoiatria', 'serviceDescription' => 'La corona serve a migliorare l’aspetto del tuo sorriso e a conferire maggiore robustezza a un dente indebolito o sottoposto a ricostruzione. Viene cementata sul dente limato.
            La classica corona in metallo-ceramica è stata ormai sostituita dalla versione in sola ceramica, per un risultato ancora più naturale ed estetico. Il ponte è una protesi usata per sostituire uno o più denti mancanti o che è stato necessario estrarre.
            Ogni ponte è formato da più corone: quelle centrali sostituiscono i denti mancanti mentre le più esterne vengono fissate sui denti adiacenti alla fessura – che per questo motivo vengono ridotti o moncati – facendo da pilastro.
            Proprio per evitare di dover intervenire anche su questi denti laterali, quando possibile si preferisce ricorrere a interventi di implantologia al posto del ponte.', 'img' => '/img/odontoiatria.jpeg', 'serviceDescription2' => 'Se hai una carie, l’otturazione serve a riparare il dente danneggiato e a impedire che la lesione si propaghi. Dopo aver tolto il materiale carioso, il tuo dentista applica sul dente una sostanza che ne ripristina la forma e la struttura originaria.
            A seconda di quanto è profonda la lesione, può essere necessaria l’anestesia locale. Una carie profonda può arrivare a interessare anche la polpa del dente, infiammandola o facendola andare in necrosi: in questo caso sono proprio le fibre nervose presenti nella polpa a farti avvertire quella fastidiosa ipersensibilità localizzata.
            Attraverso la devitalizzazione, detta anche cura canalare, la polpa dentale viene asportata, evitando che l’infiammazione dia origine a un ascesso o a un granuloma. L’intervento viene eseguito in anestesia locale.'],
            ['title' => 'Odontoiatria estetica e ricostruttiva', 'serviceDescription' => 'La vita di ogni giorno sottopone la nostra dentatura a molti fattori che ne possono determinare la decolorazione; basti pensare al caffè o al fumo di sigaretta.
            Lo sbiancamento dentale restituisce luminosità al tuo sorriso, rendendo i denti più bianchi senza intaccarne in alcun modo la struttura: è un intervento estetico e conservativo che ti garantisce risultati straordinari in modo totalmente indolore. Questi sottilissimi gusci in ceramica si applicano sulla superficie esterna dei denti frontali per mascherarne le imperfezioni: sono una soluzione rapida ed efficace per nascondere le macchie e i segni di usura, riallineare una dentatura sfasata o coprire quegli antiestetici spazi tra un dente e l’altro.
            Lo smalto consente un’ottima adesione senza bisogno di limare il dente per ridurne lo spessore.', 'serviceDescription2' => 'Se hai una carie o una vecchia otturazione da sostituire, scegli col tuo dentista un’otturazione estetica: è totalmente invisibile e dona al sorriso un aspetto più sano. Dopo aver rimosso la carie, si riempie la cavità e si procede strato per strato, secondo l’esatta cromia della tua dentatura: gli strati hanno diverse gradazioni, fino all’ultimo che è quasi trasparente, proprio come lo smalto naturale. L’otturazione viene poi lucidata per rendere la superficie liscia e naturale. Quando un dente è seriamente compromesso dalla carie, questi cappucci in resina o ceramica sono una valida alternative alle otturazioni: a differenza di queste ultime, infatti, non vengono modellate all’interno della bocca ma sulla base delle impronte rilevate in laboratorio.
            Bastano due sedute: nella prima si prepara il dente e si prende l’impronta; nel secondo appuntamento si esegue l’applicazione vera e propria. L’inlay serve nel caso di una ricostruzione parziale mentre l’onlay si estende su una porzione più vasta del dente.', 'img' => '/img/odontoiatria estetica.jpeg'],
            ['title' => 'Apparecchi', 'serviceDescription' => 'Se vuoi avere un sorriso sano, ricorda di sottoporti regolarmente alla pulizia e alla lucidatura dei denti: è il modo migliore per prevenire le principali patologie del cavo orale, come carie, gengiviti o piorrea.
            Lavarsi i denti, infatti, può non bastare per rimuovere in profondità la placca, il tartaro e i batteri che si formano nei punti più difficili da raggiungere con lo spazzolino. Nel caso la gengiva si riassorba e cali, questo intervento di microchirurgia permette di farla tornare nella posizione ideale.
            Le tecniche rigenerative prevedono che in alcuni casi bastino delle microincisioni, mentre altre volte si ricorre a innesti di piccole porzioni di tessuto, con le quali riabilitare la gengiva.', 'serviceDescription2' => 'Questo trattamento non chirurgico sotto anestesia rimuove la placca e i residui che si depositano tra la radice e la gengiva, smussando inoltre le irregolarità.
            Si impedisce così che il solco gengivale formi una tasca dove i batteri possano proliferare. Se hai una predisposizione a questa patologia ti consigliamo di sottoporti al trattamento con una certa regolarità:l’accumulo di tartaro può infatti infiammare e indebolire la gengiva, col progressivo scollamento dal dente. Riallineando i denti, l’apparecchio ortodontico ti aiuta a migliorare la respirazione, la masticazione e la deglutizione, oltre naturalmente a rendere più bello il tuo sorriso.
            L’apparecchio esterno, fisso o mobile, è perfetto per i pazienti di tutte le età. Gli attacchi possono essere in metallo o ceramica: in quest’ultimo caso, essendo di un colore simile al dente, l’apparecchio si nota molto meno.', 'img' => '/img/apparecchi.jpeg'],
        ];

        foreach ($services as $service) {
            if ($service['title']==$title){
            return view('dettaglioServizi', compact('service'));
            }
        }



    }

    public function staff() {

        $doctors =[
            ['name' => 'Marco', 'surname' => 'Rossi', 'role' => 'Medico Chirurgo Odontoiatra', 'img' => '/img/marcoRossi.jpeg', 'detail' => 'Marco Rossi si laurea con lode in odontoiatria e protesi dentaria nel 1999 presso l’Università degli Studi di Ferrara. Frequenta numerosi corsi specialistici nell’ambito dell’endodonzia, della chirurgia e della protesi dentaria; dal 2000 si dedica all’attività clinica. Dopo alcune collaborazioni con importanti studi odontoiatrici, nel 2000 entra nel prestigioso studio del Prof. Camillo Curioni, rilevandone in seguito l’attività.'],
            ['name' => 'Francesco', 'surname' => 'Bianchi', 'role' => 'Odontotecnico', 'img' => '/img/francescoBianchi.jpeg', 'detail' => 'Dopo la laurea in odontoiatria e protesi dentaria conseguita nel 2003 presso l’Università di Verona, Francesco Alessandro Raniolo frequenta il reparto di chirurgia maxillo-facciale di Vicenza fino al 2008. Nel frattempo consegue il master in ortodonzia a Trieste col Dr. Giuseppe Galassini (2005) e a Bologna col Dr. Corrado Ghidini (2007), entrambi con tecnica bio-progressiva.'],
            ['name' => 'Elisabetta', 'surname' => 'Gialli', 'role' => 'Assistente', 'img' => '/img/elisabettaGialli.jpeg', 'detail' => 'Laureata con lode in medicina e chirurgia nel 1979 presso l’Università di Ferrara, Elisabetta Tartari si specializza con lode in odontostomatologia all’Università di Bologna (1984) e in ortodonzia all’Università di Ferrara (2010). Inizia l’attività clinica nel 1980 sia come libera professionista, sia come allievo interno e professore a contratto presso la clinica odontoiatrica dell’Università di Ferrara.'],
            ['name' => 'Chiara', 'surname' => 'Viola', 'role' => 'Segretaria', 'img' => '/img/chiaraViola.jpeg', 'detail' => 'Chiara Viola si laurea con lode ed encomio in odontoiatria e protesi dentaria nel 1998 presso l’Università di Ferrara. Lo stesso anno supera l’esame per l’abilitazione all’esercizio della professione col massimo dei voti; inizia quindi a collaborare all’attività didattica in ortognatodonzia al corso di laurea in odontoiatria e protesi dentaria e alla scuola di specializzazione in ortognatodonzia dell’Università di Ferrara.'],
        ];

        // foreach ($doctors as $doctor){
        //     if ($doctor['name']==$name){
        //         return view('staff', compact('doctor'));
        //         }
        // }

        return view('staff', compact('doctors'));

    }

    public function doveSiamo(){
        return view('doveSiamo');
    }

    public function contatti(){
        return view('contatti');
    }

    public function contactSubmit(request $request){

        $contact = Contact::create([
            'email'=>$request->input('email'),
            'user'=>$request->input('user'),
            'message'=>$request->input('message'),
        ]);


        // $email = $request->input('email');
        // $user = $request->input('user');
        // $message = $request->input('message');
        // $contact = compact('user', 'message');

        // Mail::to($email)->send(new ContactMail($contact));

        return redirect(route('contatti'))->with('message', 'Grazie per averci contattato. Ti risponderemo al più presto.');
    }
}

