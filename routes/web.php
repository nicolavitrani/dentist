<?php

use App\Http\Controllers\PublicController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PublicController::class, 'homepage'])->name('homepage');

Route::get('/staff', [PublicController::class, 'staff'])->name('staff');

Route::get('/doveSiamo', [PublicController::class, 'doveSiamo'])->name('doveSiamo');

Route::get('/contatti', [PublicController::class, 'contatti'])->name('contatti');

Route::get('/dettaglioServizi/{title}', [PublicController::class, 'dettaglioServizi'])->name('dettaglioServizi');

Route::post('/contatti/submit', [PublicController::class, 'contactSubmit'])->name('contacts.submit');
